<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 48(POST)</title>
</head>
<body>
    <?php
        $errors = [];
        if(!empty($_POST)){
            if(empty($_POST['first'])){
                $errors[] = 'Текстове поле не заповнено';
            }    
            if(empty($errors)){
                echo htmlspecialchars($_POST['first']);
                exit;
            }               
        }
        if(!empty($errors)){
            foreach($errors as $err){
                echo "<span style='color:red'>$err</span><br>";
            }
        }
    ?>

    <form method="post">
        <input type="text" name="first" value="<?php
            htmlspecialchars($_POST['first'], ENT_QUOTES);
        ?>"><br>
        <input type="submit" value="Відправити">
    </form>
</body>
</html>