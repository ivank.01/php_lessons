<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 35</title>
</head>
<body>
    <?php
        $arr = [5=>1,'2',3];
        for($i=0; $i<10; $i++){
            if(isset($arr[$i])){
                echo "Елемент \$arr[$i] масива існує <br>";
            }else{
                echo "Елемент \$arr[$i] масива не існує <br>";
            }
        }

        if(is_array($arr)){
            echo "Це масив <br>";
        }else{
            echo "Це не масив <br>";
        }

        if(in_array(2, $arr, true)){
            echo "Значення 2 знайдено в масиві <br>";
        }else{
            echo "Значення 2 не знайдено в масиві <br>";
        }

        if(array_key_exists(5, $arr)){
            echo "Ключ 5 знайдено в масиві <br>";
        }else{
            echo "Ключ 5 не знайдено в масиві <br>";
        }

        echo array_search(1, $arr); //шукає ключ по значенню
        echo "<br>";

        unset($arr[6]); //видаляє елемент масиву
        echo "<pre>";
        print_r($arr) ;
        echo "</pre>"; 
    ?>
</body>
</html>