<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 15</title>
</head>
<body>
    <?php
        // echo 'Шлях до файлу ' . __DIR__ . '<br>';
        // require_once'../lesson13/point.php';

        //require_once __DIR__ . '/../lesson13/point.php';

        class ConstClass{
            const NAME = 'str';
        }
        if(defined('ConstClass::NAME')){
            echo "Константа визначена";
        } 
        else {
            echo "Константа не визначена";
        }
    ?>
</body>
</html>