<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 54</title>
</head>
<body>
    <?php
        class Animal{
            protected $legs = 4;
            public function info(){
                echo "У мене {$this->legs} лапи. ";
            }
        }

        class Dog extends Animal{
            public $name = "Пес";
            public function info(){
                echo "Я {$this->name}. У мене {$this->legs} лапи. ";
            }
            public function voice(){
                echo "{$this->name} говорить гав-гав<br>";
            }
            public function parentInfo(){
                parent::info();
            }
        }
        $dog = new Dog;
        $dog->info();
        $dog->voice();

        class Cat extends Animal{
            public $name = "Кіт";
            public function voice(){
                echo "{$this->name} говорить няв<br>";
            }
        }

        $cat = new Cat;
        $cat->info();
        $cat->voice();

        $dog->parentInfo();
    ?>
</body>
</html>