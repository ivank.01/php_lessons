<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 52</title>
</head>
<body>
    <?php
        class Hello{
            public function print_Text(){
                return "Hello World!";
            }
        }
        $obj = new Hello;
        echo $obj->print_Text() . '<br>';

        class Point{
            private $x;
            private $y;
            public function setX($x){
                $this->x=$x;
            }
            public function setY($y){
                $this->y=$y;
            }
            public function getX(){
                return $this->x;
            }
            public function getY(){
                return $this->y;
            }
            public function distance(){
                return sqrt($this->getX()**2 + $this->getY()**2 );
            }
        }
        $p1 = new Point;
        $p1->setX(14);
        $p1->setY(8);
        echo $p1->distance() . '<br>';   

        class Hello1{
            public static function print_Text(){
                return "Hello World!";
            }
        }
        echo Hello1::print_Text() . '<br>';

        class Page{
            static $content = 'Тіло сайту<br>';
            public static function footer(){
                return 'Це підвал сайту<br>';
            } 
            public static function header(){
                return 'Це шапка сайту<br>';
            } 
            public static function site(){
                echo self::header() . self::$content . self::footer();
            }
        }

        Page::site();
    ?>
</body>
</html>