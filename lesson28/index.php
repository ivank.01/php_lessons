<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 28</title>
</head>
<body>
    <?php
        $var = "wow it's amazing";
        $arr1 = (array)$var;
        echo "<pre>";
        print_r($arr1);
        echo "</pre>";

        $arr = ['Hello', 'world', '!'];
        $arr[6]= 'Bob';
        // echo "$arr[1]";
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
    ?>
</body>
</html>