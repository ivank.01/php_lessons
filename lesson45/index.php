<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 45</title>
</head>
<body>
    <?php
        printf("Перше число - %d", 25);
        echo '<br>';

        $number = 5867;
        printf('Двоичное число : %b<br />', $number);
        printf('Десятичное число: %d<br />', $number);
        printf('Число с плавающей точкой: %f <br / >', $number);
        printf('Восьмеричное число: %o<br />', $number);
        printf('Строковое представление: %s <br / >', $number);
        printf('Шестнадцатеричное число (нижний регистр): %x<br / >', $number);
        printf('Шестнадцатеричное число (верхний регистр): %X<br / >', $number); 

        $red = 255;
        $green = 255;
        $blue = 100;
        printf('#%X%X%X', $red, $green, $blue);

        echo '<pre>';
        printf('%4d', 42);
        echo '<br>';
        printf('%04d', 42);
        echo '</pre>';

        $str = "Ім'я, Прізвище, Телефон";
        echo '<pre>';
        print_r(explode(', ' , $str));
        echo '</pre>';

        $arr = ["Ім'я", "Прізвище", "Телефон", "e-mail"];
        echo implode(" ", $arr);
    ?>
</body>
</html>