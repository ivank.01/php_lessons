<?php
    if(($_FILES['filename']['size'] > 3*1024*1024)){
        exit("Розмір файлу перевищує 3 мегабайти");
    }

    if(move_uploaded_file($_FILES['filename']['tmp_name'], 'temp\\'.$_FILES['filename']['name'])){    
        echo "Файл успішно завантажено" . '<br>';
        echo "Ім'я файлу - " . $_FILES['filename']['name'] . '<br>';
        echo "Розмір файлу в байтах - " . $_FILES['filename']['size'] . '<br>';
        echo "Тип файлу - " . $_FILES['filename']['type'] . '<br>';
        echo "Тимчасовий файл, в якому збережений завантажений файл - " . $_FILES['filename']['tmp_name'] . '<br>';
    }else{
        echo "Помилка завантаження файлу";
    }
?>