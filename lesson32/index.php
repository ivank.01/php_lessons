<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 32</title>
</head>
<body>
    <?php
        $arr =[1,2,3];
        list($one, $two, $three) = $arr; //розбирає масив на змінні
        echo $one . "<br>";
        echo $two . "<br>";
        echo $three . "<br>";

        $x = 12;
        $y = 6;
        echo "до: <br>";
        echo "x:$x <br>";
        echo "y:$y <br>";

        list($y, $x) = [$x, $y]; //міняє місцями змінні

        echo "після: <br>";
        echo "x:$x <br>";
        echo "y:$y <br>";
    ?>
</body>
</html>