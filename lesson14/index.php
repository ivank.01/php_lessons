<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 14</title>
</head>
<body>
    <?php
        if (define('NUMBER', 16, true)){ //створення константи
            echo "Константа NUMBER успішно створена і має значення 16";
        }
        if (defined('NUMBER')){ //перевіряє чи існує константа та передає значення true or false
            echo "Константа NUMBER вже створена";
        }
        
        //echo NUMBER;

        $num = mt_rand(0,10);
        $name = "VALUE($num)";
        define($name, $num);
        echo constant($name);  
    ?>
    <br>
    <?php
        echo "Ім'я файлу " . __FILE__ . '<br>';
        echo "Рядок " . __LINE__ . '<br>';
    ?>
</body>
</html>