<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 30</title>
</head>
<body>
    <?php
        $transport = [
            'Авто' => ['BMW', 'Mazda', 'Toyota'],
            'Літаки' => ['A-22', 'НАРП-1', 'Стрепет'],
            'Кораблі' => ['Авіаносець', 'Яхта', 'Шлюп']  //багатовимірний масив
        ];
        echo "<pre>";
        print_r($transport);
        echo "</pre>";
        echo $transport['Авто'][2];     
    ?>
</body>
</html>