<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Урок 51</title>
</head>
<body>
    <form action="sendmail.php" method="POST">
        <select name = "subject">
            <option disabled selected>Тема листа</option>
            <option value="1">Питання по уроку</option>
            <option value="2">Особисте питання</option>
            <option value="3">Подяка</option>
        </select>
        <input type="email" name="enail" placeholder="Введіть ваш e-mail" maxlength="45" required>
        <textarea name="message" placeholder="Введіть повідомлення" maxlength="150" required></textarea>
        <img src="capcha.jpg" height="25" width="60">
        <input type="number" name="capcha" placeholder="Введіть відповідь" maxlength="3">
        <input type="submit" value="Відправити лист">
    </form>
</body>
</html>