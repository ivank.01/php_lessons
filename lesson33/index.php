<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 33</title>
</head>
<body>
    <?php
        $number = ['1', '2', '3', '4', 'hello', true, 51];
        for($i=0; $i<count($number); $i++){
            echo "$number[$i] <br>";
        }
        
        $arr = [
            'auto' => 'bmw',
            'ship' => 'Blach Pear',
            'plane' => 'IL-2'
        ];
        foreach($arr as $key => $value){
            echo "$key : $value <br>";
        }
        $transport = [
            'Авто' => ['BMW', 'Mazda', 'Toyota'],
            'Літаки' => ['A-22', 'НАРП-1', 'Стрепет'],
            'Кораблі' => ['Авіаносець', 'Яхта', 'Шлюп']  
        ];
        foreach($transport as $key => $array){
            echo "<b>$key</b>";
            // foreach($array as $value){
            //     echo "<li>$value</li>";
            // }
            for($j=0; $j<count($array); $j++){
                echo "<li>$array[$j]</li>";        
            }
        }    
    ?>
</body>
</html>