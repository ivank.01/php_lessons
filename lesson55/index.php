<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 55</title>
</head>
<body>
    <?php

use Animal as GlobalAnimal;

        abstract class Animal{
            protected $legs = 4;
            final public function info(){
                echo "У мене {$this->legs} лапи. ";
            }
            abstract public function color();
        }

        class Dog extends Animal{
            public function color(){
                echo "red";
            }
            public $name = "Пес";
            // public function info(){
            //     echo "Я {$this->name}. У мене {$this->legs} лапи. ";
            // }
            public function voice(){
                echo "{$this->name} говорить гав-гав<br>";
            }
            public function parentInfo(){
                parent::info();
            }
        }

        $dog = new Dog;
        $dog->parentInfo();
        $dog->color();
        if($dog instanceof Animal){
            echo "<br> Dog є екземпляром класу Animal";
        }
    ?>
</body>
</html>