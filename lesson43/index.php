<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 43</title>
</head>
<body>
    <?php
        $date = "05.06.2003";
        echo 'день ' . substr($date, 0, 2) . '<br>'; 
        echo 'місяць ' . substr($date, 3, 2) . '<br>'; 
        echo 'рік ' . substr($date, 6) . '<br>'; 

        $str = "Hello world";
        echo strpos($str, 'world');

        $str1 = "Php - [b]інтерпретована[/b] мова, а це [b]жирний текст[/b]";
        echo $str1 . '<br>';
        $str1 = str_replace('[b]' , '<b>' , $str1);
        $str1 = str_replace('[/b]' , '</b>' , $str1, $number);
        echo $str1 . '<br>';
        echo "Зроблено замін: " . $number*2 . '<br>';

        $str2 = " Hello  World ";
        echo strlen($str2) . '<br>'; 
        echo strlen(trim($str2)) . '<br>'; // trim прибирає пробіли
        echo trim($str2, "  He!");
    ?>
</body>
</html>