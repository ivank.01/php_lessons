<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 23</title>
</head>
<body>
    <?php
        $char = 'php';
        switch ($char){
            case 'php':
        ?>            
                <h2>Мова php</h2>    
            <?php            
            break;
        case 'js':
            ?>            
                <h2>Мова JavaScript</h2>
            <?php  
            break;  
        case 'c#':
            ?>            
                <h2>Мова C#</h2>
            <?php  
            break; 
        default:
            ?>
            <h2>Невідома мова</h2>    
            <?php
    }       
    ?>
    <br>
    <?php
        $number = 120;
        switch(true){
            case($number > 0 && $number <=10):
                echo "$number менше 10 та більше 0";
                break;
            case($number > 10 && $number <=100):
                echo "$number менше 100 та більше 10";
                break; 
            case($number > 100 && $number <=1000):
                echo "$number менше 1000 та більше 100";
                break;
            default:
                echo "$number більше 1000 та менше 0";    
                break;
        }
        ?>
</body>
</html>