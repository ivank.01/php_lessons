<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 10</title>
</head>
<body>
    <?php
        echo round(52.6552047, 2);   // round() заокруглює число
    ?><br>    
    <?php
        echo ceil(2.4111);   //заокруглює дроби до більшого значення
    ?>
    <br>    
    <?php
        echo floor(25.6);   //заокруглює дроби до меншого значення
    ?>
</body>
</html>