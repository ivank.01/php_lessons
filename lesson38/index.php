<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 38</title>
</head>
<body>
    <?php
    function mySum(int $a, int $b) : int {
        return $a + $b;
    }

    echo mySum(2.5, 5,9);
    echo '<br>';
    
    function sum($b, $a = 10){
        return $a + $b;
    }
    echo sum(10,12);
    echo '<br>';

    function outArguments(...$items){
        foreach($items as $arg){
            echo "$arg<br>";
        }
    }
    outArguments('php', 'c++', 'java', 'js');


    function outArguments1($c, $d, $x, $y){
        echo "$c<br>";
        echo "$d<br>";
        echo "$x<br>";
        echo "$y<br>";
    }
    $items1 =['PHP', 'C++', 'JAVA', 'JS'];
    outArguments1(...$items1);

    ?>
</body>
</html>