<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 40</title>
</head>
<body>
    <?php

use Point as GlobalPoint;

        function recursiya($counter){
            if($counter>0){
                echo $counter-- . '<br>';
                recursiya($counter);
            } else return;
        }
        recursiya(8);    
    ?>
    <?php
        function outter(){
            function inner(){
                return "Hello World!";
            }
        }
        outter();
        echo inner() . '<br>';
    ?>
    <?php
        function first(){
            return "First Function!";
        }
        function second(){
            return "Second Function!";
        }
        $newFunction = mt_rand(0,1) ? first() : second();
        echo $newFunction;
    ?>
    <?php
        class Point{
            public $x;
            public $y;
        }
        $fst = new Point;
        $fst->x=12;
        $fst->y=2;

        $sec = new Point;
        $sec->x=9;
        $sec->y=5;

        $thd = new Point;
        $thd->x=7;
        $thd->y=1;

        $arr = [$fst, $sec, $thd];

        usort($arr, function($a, $b){
            $dist_a = sqrt($a -> x ** 2 + $a -> y ** 2);
            $dist_b = sqrt($b -> x ** 2 + $b -> y ** 2);
            return $dist_a <=> $dist_b;
        });
        echo '<pre>';
        print_r($arr);
        echo '</pre>';

    ?>

</body>
</html>