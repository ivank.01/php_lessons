<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 53</title>
</head>
<body>
    <?php

use Point as GlobalPoint;

    class People{
        private $name;
        public function __construct()
        {
           echo "Виклик конструктора";
           $this->name = "Ivan"; 
        }
    }
    $obj = new People;
    echo '<pre>';
    print_r($obj);
    echo '</pre>';

    class Point{
        private $x;
        private $y;
        public function __construct($x=0, $y=0)
        {
            $this->x=$x;
            $this->y=$y;
        }
        public function __toString()
        {
            return "({$this->x}, {$this->y})";
        }
    }

    $obj1 = new Point(10,20);
    $obj2 = new Point(11,19);
    echo '<pre>';
    print_r($obj1);
    print_r($obj2);
    echo '</pre>';
    echo "{$obj1}";
    ?>
</body>
</html>