<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 39</title>
</head>
<body>
    <?php
        function sum(){
            global $var; //add global
            $var = 219;
            return $var;
        }
        $var = 30;        
        echo "$var<br>";
        echo sum();
        echo "<br>$var<br>";

        function sum1(){
            static $count = 0;
            return ++$count;
        }
        echo sum1() . '<br>';
        echo sum1() . '<br>';
        echo sum1() . '<br>';
        echo sum1() . '<br>';

        function formatSize($bytes){
            $kbytes = $bytes / 1024; 
            $mbytes = $kbytes / 1024; 
            $gbytes = $mbytes / 1024; 
            return [$bytes, $kbytes, $mbytes, $gbytes]; //повертаємо масив
        }

        echo "<pre>";
        print_r(formatSize(5441233017));
        echo "</pre>";
        list($bytes, $kbytes, $mbytes, $gbytes) = formatSize(5441233017); // перевели масив в змінні
        echo "$bytes<br>$kbytes<br>$mbytes<br>$gbytes"; 

    ?>
</body>
</html>