<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 8</title>
</head>
<body>
    <?php
        $user = "Ivan";
        if (isset($user)){           // isset перевіряє чи змінна існує та відмінна від значення NULL
            echo "Змінна існує";
        }
        else
            echo "Змінна не існує";
    ?>
    <br>
    <?php
        $str = "";
        if(empty($str)){            // empty перевіряє чи пуста строка
            echo "Рядок пустий";
        }
        else
            echo "В рядку щось є";
    ?>
    <br>
    <?php
        echo gettype(1.23);       //визначає тип даних; is_int is_double is_string - чи є тим чи іншим типом змінна
    ?>                            
</body>
</html>