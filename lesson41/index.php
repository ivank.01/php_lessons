<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 41</title>
</head>
<body>
    <?php
        $message = "Текстове повідомлення";
        $mFu = function() use ($message){ // замикання
            $message = "New";
            return $message;
        };
        echo $mFu() . '<br>'; 
        echo $message . '<br>';
    ?>
    <?php
        function odd(int $number){
            if($number % 2 == 0){
                return false;
            } else return true;
    }
        echo odd(9) . '<br>';
    ?>
    <?php
        function sum(...$items){
            $sum = 0;
            for($i = 0; $i<count($items); $i++){
                $sum += $items[$i];
            }
            return $sum;
        }
        echo sum(10,2,10,2,6);
    ?>
</body>
</html>