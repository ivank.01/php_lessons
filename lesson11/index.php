<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 11</title>
</head>
<body>
     <?php
        require 'point.php';
        $point1 = new Point();
        $point1->x = 13;
        $point1->y = 8;
        echo $point1->x;

        $point2 = new Point();
        $point2->x = 10;
        $point2->y = 2;
        echo $point2->x;

        unset ($point2);  //delete
        echo $point2->y;
     ?>
</body>
</html>