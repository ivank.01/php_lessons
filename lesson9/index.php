<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Урок 9</title>
</head>
<body>
    <?php
        $str = '54.223';
        $number = $str - 12;
        echo $number;   //неявне приведення типів
    ?>
    <br>
    <?php
        $float = 4.6;
        $num = (int)$float;
        echo $num;    //явне приведення
    ?>
    <br>
    <?php
        $a = 32;
        $b = (float)($a/2) - (int)($a/2);
        if($b){
            echo "Число непарне";
        }
        else
            echo "Число парне";
    ?>
</body>
</html>